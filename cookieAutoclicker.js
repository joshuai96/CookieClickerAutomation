// ==UserScript==
// @name         JI_Cookie_Autoclicker
// @namespace    http://tampermonkey.net/
// @version      0.2.1
// @description  All the cookies in the world
// @author       JI
// @match        http://orteil.dashnet.org/cookieclicker/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var topbar = document.getElementById('topBar');

    var checkerwrapper = document.createElement('div');
    // ui for auto clicker
    var checker = document.createElement('input');
    checker.setAttribute("type", "checkbox");
    checker.setAttribute("id", "JI_Clicker");

    var interval_text = document.createTextNode('Intervall: ');
    var clicker_text = document.createTextNode('Clicker ');

    var input = document.createElement('input');
    input.setAttribute('type', 'number');
    input.setAttribute('min', '10');
    input.setAttribute('value', '10');
    input.setAttribute('id', 'JI_Intervall');
    input.setAttribute('style', 'width: 30px;');

    var hidden_input = document.createElement('input');
    hidden_input.setAttribute('type', 'hidden');
    hidden_input.setAttribute('id', 'JI_hidden');

    checkerwrapper.appendChild(clicker_text);
    checkerwrapper.appendChild(checker);
    checkerwrapper.appendChild(interval_text);
    checkerwrapper.appendChild(input);
    checkerwrapper.appendChild(hidden_input);
    topbar.appendChild(checkerwrapper);

    // activate auto clicker mechanism
    checker.addEventListener('change', function() {
        var checked = this.checked;
        var $input = document.getElementById('JI_Intervall');
        var $hidden = document.getElementById('JI_hidden');
        var timer = null;

        if (checked == true) {
            var interval = $input.value;
            $input.setAttribute('readonly', 'readonly');
            timer = setInterval(function () {
                document.getElementById("bigCookie").click();
            }, interval);
            $hidden.setAttribute('value', timer);
        } else {
            $input.removeAttribute('readonly');
            timer = $hidden.value;
            clearInterval(timer);
        }
    });

    // golden cookie auto detect and click
    var golden_wrapper = document.createElement('div');
    var golden_checker = document.createElement('input');
    golden_checker.setAttribute('type', 'checkbox');
    golden_checker.setAttribute('id', 'JI_gold_clicker');

    var golden_label = document.createTextNode('Gold Cookie Clicker');

    var golden_hidden = document.createElement('input');
    golden_hidden.setAttribute('type', 'hidden');
    golden_hidden.setAttribute('id', 'JI_golden_hidden');

    golden_wrapper.append(golden_label);
    golden_wrapper.append(golden_checker);
    golden_wrapper.append(golden_hidden);

    topbar.append(golden_wrapper);

    // activate golden auto click mechanism
    golden_checker.addEventListener('change', function() {
        var checked = this.checked;
        var $hidden = document.getElementById('JI_golden_hidden');
        var timer = null;

        if (checked == true) {
            timer = setInterval(function () {
                var $children = document.getElementById('shimmers').children;

                for (var i = 0; i < $children.length; i++) {
                    $children[i].click();
                }
            }, 1000);
            $hidden.setAttribute('value', timer);
        } else {
            timer = $hidden.value;
            clearInterval(timer);
        }
    });
})();